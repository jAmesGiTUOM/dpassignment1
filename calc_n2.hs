calc_n2::[Int]->[Int]->Int
calc_n2 [] _ =0
calc_n2 (x:xs) y
               | notFound = calc_n2 xs y
               | otherwise = 1+calc_n2 xs y'
               where y'=calc_n2' x y
                     notFound=(length y' == length y)

calc_n2'::Int->[Int]->[Int]
calc_n2' n [] = []
calc_n2' n (x:xs)
               | n==x = xs
               | otherwise = x:calc_n2' n xs
