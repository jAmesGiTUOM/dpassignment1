--  File     : Proj1.hs
--  Author   : JingCheng Wang 691808
--  Purpose  : The program for Project 1 of COMP90048
--  Workflow : The program will output an initial guess to the test program,
--             then take the feedback from the test program to produce
--             a new round of outputs. Until the test program prompts that
--             the output guess is correct.

module Proj1 (initialGuess, nextGuess, GameState) where
import Data.List

-- The game state stores all the possible targets we can guess
-- It will be reduced each time after a guess based on the feedback
type GameState = ([[String]])

-- The score list stores a list of all the scores when comparing a target
-- to all the other target
type ScoreList = [(Int,Int,Int)]

-- | The "best" initial guess is found by 'experienmenting', the idea
-- is essentially to find the guess that will result in minimum number
-- of possible targets for the next guess.
-- Since I already have a function that calculates the maximum number of targets
-- any targets will leave in a certain game state.
-- I would only need to find the one that has the minimum
-- of this number in the initial game state.
-- and the result is ["A1","B2","C1"],with a maximum of 196 targets left
initial_guess = ["A1","B2","C1"]

-- The initial game state would contain all possible guess combinations
initial_state = generate_all_target

-- | initialGuess function takes no input and returns the initial guess
-- and the initial GameState to the test program
initialGuess::([String],GameState)
initialGuess=(initial_guess,initial_state)

-- | nextGuess function takes in the previous guess,a gamestate and
-- the feedback,
-- it will then firstly reduce the gamestate(possible guesses) based on the
-- feedback, then choose from the new gamestate for a best next guess,then
-- pass it into the test program for next round of feedbacks.The magic number
-- 1330 will be explained in choose_next_guess's function comments
nextGuess::([String],GameState)->(Int,Int,Int)->([String],GameState)
nextGuess result score  = (new_guess,new_state)
          where new_guess =  choose_next_guess new_state new_state max_state []
                new_state =  eliminate_target result score
                state = snd result
                max_state = 1330
-- | eliminate_target function take a guess,a gamestate and a feedback, to
-- find the guesses in that gamestate, whose score with the input guess,
-- is equal to the feedback. Then return a list of all matching guesses
eliminate_target::([String],GameState)->(Int,Int,Int)->GameState
eliminate_target (guess,gamestate) score = [x
                                           | x<-gamestate
                                           ,(calc_score guess x)==score]

-- | choose_next_guess takes in two gamestates;one is the list that need to be
-- calculated for remaining target number,another one is the whole list for
-- calculation;the current minimum for number of possible guesses;
--  and a current guess that has the minumum remaining guesses. It computes the
-- remaining number of possible guesses for every element in the list,then
-- return the minimum one
choose_next_guess::GameState->GameState->Int->[String]->[String]
choose_next_guess [] _ _ guess = guess
choose_next_guess (x:xs) list mini guess
                        | new_mini < mini = choose_next_guess xs list new_mini x
                        | otherwise = choose_next_guess xs list mini guess
                        where new_mini = calc_remaining list x
-- | Takes in a GameState and a guess, compute the maximum remaining
-- possible targets for that guess in the gamestate,then return the number
-- the function will rely on a helper function calc_remaining' to scan through
-- the scorelist of the guess and find the maximum one. The helper
-- function requires the scorelist to be sorted, and only calculate one specific
-- score once(using nub) will speed up the process
calc_remaining::GameState->[String]->Int
calc_remaining list target=calc_remaining' (nub scorelist) (sort scorelist) 0
                          where scorelist = calc_all_score list target

-- | takes in a gamestate and a guess, then calculates the guess' score against
-- all other guesses,then return all the scores as a list
calc_all_score::GameState->[String]->ScoreList
calc_all_score [] _ = []
calc_all_score (x:xs) target = (calc_score x target):(calc_all_score xs target)

-- | The helper class for calc_remaining, it takes the scorelist for scanning,
-- and the whole scorelist for calculation,then returns the number of times
-- for the score that has the most appearance
calc_remaining'::ScoreList->ScoreList->Int->Int
calc_remaining'  [] _ max_score = max_score
calc_remaining'  (x:xs) list max_score
                        | current_score > max_score = update_score
                        | otherwise  = no_update_score
                        where current_score = calc_appearance_for_one x list
                              update_score=calc_remaining' xs list current_score
                              no_update_score=calc_remaining' xs list max_score

-- | takes in a score and a scorelist,compute the number of appearances of that
-- score in the list.
calc_appearance_for_one::(Int,Int,Int)->[(Int,Int,Int)]->Int
calc_appearance_for_one _ [] = 0
calc_appearance_for_one x (y:ys)
                               | x==y = 1+calc_appearance_for_one x ys
                               | otherwise = calc_appearance_for_one x ys

-- | takes in two guesses,and check if they are equal, the order doesn't matter
are_two_guesses_equal::[String]->[String]->Bool
are_two_guesses_equal [] _ = True
are_two_guesses_equal a b
                      | (head a) == (b!!0) = checkNext
                      | (head a) == (b!!1) = checkNext
                      | (head a) == (b!!2) = checkNext
                      | otherwise = False
                      where checkNext=are_two_guesses_equal (tail a) b
-- | generates all the pitches as a list of strings
generate_all_pitch::[String]
generate_all_pitch=[a:b:[]|a<-['A'..'G'],b<-['1'..'3']]

-- | based on the all the possible pitches,generate all the guesses(targets),
-- combination with the same the elements but different order needs to be
-- removed
generate_all_target::[[String]]
generate_all_target = [a:b:c:[]|a<-list,b<-list,c<-list,a<b&&b<c]
                      where list=generate_all_pitch

-- | takes in a guess and a list of guesses, remove the guess from the list
-- then return the updated list
remove_target::[String]->[[String]]->[[String]]
remove_target target list =[x |x<-list,not (are_two_guesses_equal target x)]

-- | takes in a pitch and a guess, then checks if the pitch exists in the guess
-- return the result in boolean
contains_pitch::String->[String]->Bool
contains_pitch _ [] = False
contains_pitch pitch target
                          | pitch == (head target) = True
                          | otherwise = contains_pitch pitch (tail target)

-- | takes in two guesses, remove the common pitch(es) from the both guess then
-- return them as a tuple. When calculating the scores, this will prevent two
-- exact same pitches gets calculated for note score and octa score
remove_same_target::[String]->[String]->([String],[String])
remove_same_target x y =(new_x,new_y)
                      where new_x=filter (`notElem` y) x
                            new_y=filter (`notElem` x) y

-- | takes in two guesses, calculate the score(feedback) for them,it calls
-- the sub functions to do the calculation.
calc_score::[String]->[String]->(Int,Int,Int)
calc_score x y = (n1,n2,n3)
                 where n1 = calc_n1 x y
                       n2 = calc_n2_n3 new_x_note new_y_note
                       n3 = calc_n2_n3 new_x_octa new_y_octa
                       new_x=fst (remove_same_target x y)
                       new_y=snd (remove_same_target x y)
                       new_x_note=get_note_or_octa new_x 0
                       new_y_note=get_note_or_octa new_y 0
                       new_x_octa=get_note_or_octa new_x 1
                       new_y_octa=get_note_or_octa new_y 1

-- | takes in two guesses, find the number of common pitches,return it as an int,
-- this is the first term in a score
calc_n1::[String]->[String]->Int
calc_n1 [] y = 0
calc_n1 (x:xs) y = if contains_pitch x y
                   then 1+(calc_n1 xs y)
                   else (calc_n1 xs y)
-- | takes two lists,find the number of common notes or octas. The list should
-- contain only all notes or all octas. return the result as an int, this can be
-- the second or third term of the score. The function will compare each element
-- in the first guess to elements in the second guess, if there is a match, that
-- element will be removed by the helper function, so we check if the length
-- changes to find out if there is a match.
calc_n2_n3::[String]->[String]->Int
calc_n2_n3 [] _ = 0
calc_n2_n3 (x:xs) y
              | notFound = calc_n2_n3 xs y
              | otherwise = 1+calc_n2_n3 xs y'
              where y'=calc_n2_n3_helper x y
                    notFound=(length y' == length y)
-- | takes in an element of a guess and another guess, check if there is a match
-- if there is a match, remove the element from the guess and return the updated
-- guess. otherwise just return the original guess.
calc_n2_n3_helper::String->[String]->[String]
calc_n2_n3_helper _ [] = []
calc_n2_n3_helper target (x:xs)
                      | target==x = xs
                      | otherwise = x : calc_n2_n3_helper target xs

-- | retrieve all the notes or octas from a pitch, this function takes in a
-- guess and a parameter, when the parameter is 0 , it will return the notes as
-- a list, if the parameter is 1,it will then return the octas.
get_note_or_octa::[String]->Int->[String]
get_note_or_octa [] _ = []
get_note_or_octa (x:xs) n = [x!!n] : get_note_or_octa xs n
