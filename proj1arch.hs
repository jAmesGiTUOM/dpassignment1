is_possible::[String]->Int->[String]->(String->[String]->Bool)->Bool
is_possible test_string n target_string contain_function
                                       | n==0 = not (c0&&c1&&c2)
                                       | n==1 = (c0||c1||c2)&&(not equal)
                                       | n==2 = ((c0&&c1)||(c1&&c2)||(c0&&c2))&&(not equal)
                                       where  equal=c0&&c1&&c2
                                              c0= (contain_function (target_string!!0) test_string)
                                              c1= (contain_function (target_string!!1) test_string)
                                              c2= (contain_function (target_string!!2) test_string)

remove_targets::[String]->Int->[[String]]->(String->[String]->Bool)->[[String]]
remove_targets string n list contain_function
                        | n==0 = [x |x<-list,(is_possible x 0 string contain_function)]
                        | n==1 = [x |x<-list,(is_possible x 1 string contain_function)]
                        | n==2 = [x |x<-list,(is_possible x 2 string contain_function)]



eliminate_target::[String]->GameState->(Int,Int,Int)->GameState
eliminate_target guess current_state (n1,n2,n3)= result
                              where first_stage=remove_targets guess n1 current_state contains_pitch
                              second_stage=remove_targets guess n2 first_stage contains_note
                              result=remove_targets guess n3 second_stage contains_octa





                              min intersect1 intersect2
                                            where new_x = get_note_or_octa x 0
                                                  new_y = get_note_or_octa y 0
                                                  intersect1 = length (intersect new_x new_y)
                                                  intersect2 = length (intersect new_y new_x)

                                                  min intersect1 intersect2
                                                                where new_x = get_note_or_octa x 1
                                                                      new_y = get_note_or_octa y 1
                                                                      intersect1 = length (intersect new_x new_y)
                                                                      intersect2 = length (intersect new_y new_x)




                                                                      data Chord = Chord Char Int deriving Show
                                                                      data Guess = Guess Chord Chord Chord deriving Show

                                                                      stringlist_to_guess::[String]->Guess
                                                                      stringlist_to_guess [a,b,c] = Guess fir snd thi where
                                                                                                    fir=string_to_chord a
                                                                                                    snd=string_to_chord b
                                                                                                    thi=string_to_chord c

                                                                      string_to_chord::String->Chord
                                                                      string_to_chord string = Chord note oct where
                                                                                               note=head string
                                                                                               oct=read (tail string) :: Int




                              contains_note::String->[String]->Bool
                              contains_note _ [] =False
                              contains_note pitch target
                                                      | note == target_note = True
                                                      | otherwise = contains_note pitch (tail target)
                                                      where note = head pitch
                                                            target_note = head (head target)


                              contains_octa::String->[String]->Bool
                              contains_octa _ [] =False
                              contains_octa pitch target
                                                      | note == target_note = True
                                                      | otherwise = contains_octa pitch (tail target)
                                                      where note = pitch!!1
                                                            target_note = (head target)!!1


                                                            guess_in_collection::[String]->[[String]]->Bool
                                                            guess_in_collection _ [] = False
                                                            guess_in_collection a b = if are_two_guesses_equal a (head b) then True
                                                                                      else guess_in_collection a (tail b)
